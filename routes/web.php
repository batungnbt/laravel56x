<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function() {
	Route::get('home',function(){
		return view('home');
	});
    Route::group(['prefix' => 'student'], function() {
        Route::get('add','StudentController@getAdd');
        Route::post('add','StudentController@postAdd');
        Route::get('list','StudentController@getList');
        Route::get('delete/{id}','StudentController@delete');
        Route::get('edit/{id}','StudentController@getEdit');
        Route::post('edit/{id}','StudentController@postEdit');

    });
   	Route::group(['prefix' => 'grade'], function() {
   	    Route::get('add','GradeController@getAdd');
   	    Route::post('add','GradeController@postAdd');
   	    Route::get('list','GradeController@getList');
        Route::get('delete/{id}','GradeController@delete');
        Route::get('edit/{id}','GradeController@getEdit');
        Route::post('edit/{id}','GradeController@postEdit');
   	});
   	Route::group(['prefix' => 'class'], function() {
   	    Route::get('add','ClassController@getAdd');
   	    Route::post('add','ClassController@postAdd');
   	    Route::get('list','ClassController@getList');
        Route::get('delete/{id}','ClassController@delete');
         Route::get('edit/{id}','ClassController@getEdit');
        Route::post('edit/{id}','ClassController@postEdit');
   	});


    Route::get('ajax/class/{idGrade}','AjaxController@getClass');
    Route::get('login','UserController@getLogin');

});












