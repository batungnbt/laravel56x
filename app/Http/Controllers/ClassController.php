<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;
use App\Classs;
use Illuminate\Support\Facades\View;

class ClassController extends Controller
{
     public function __construct()
    {
        $class = Classs::all();
        $grade = Grade::all();
        View::share('class',$class);
        View::share('grade',$grade);

    }
    public function getAdd()
    {	
    	$grade = Grade::all();
    	return view('class.add',['grade' => $grade]);
    }

    public function postAdd(Request $request)
    {
    	$validateData = $request->validate([
    		'name' => 'required|unique:class,class_name',
    		'grade' => 'required'
    	],
    	[
    	'name.required' => 'Bạn chưa nhập tên lớp',
    	'name.unique' => 'Tên lớp đã tồn tại',
    	'grade.required' => 'Bạn chưa chọn khối'
    	]);

    	$class = new Classs;

    	$class->class_name = $request->name;
    	$class->grade_id = $request->grade;
    	$class->save();

    	return redirect('admin/class/add')->with('thongbao','Thêm mới thành công');
    }

    public function getList()
    {
        $class = Classs::all();
        return view('class.list',['class' => $class]);
    }

    public function delete($id)
    {
        $class = Classs::find($id);
        $class->delete();
        return redirect('admin/class/list')->with('thongbao','Xóa thành công');
    }

    public function getEdit($id)
    {
        $class = Classs::find($id);
        return view('class.edit',['class' => $class]);
    }

    public function postEdit(Request $request,$id)
    {
        $class = Classs::find($id);
        $validateData = $request->validate([
            'name' => 'required|unique:class,class_name',
            'grade' => 'required'
        ],
        [
        'name.required' => 'Bạn chưa nhập tên lớp',
        'name.unique' => 'Tên lớp đã tồn tại',
        'grade.required' => 'Bạn chưa chọn khối'
        ]);

        

        $class->class_name = $request->name;
        $class->grade_id = $request->grade;
        $class->save();

        return redirect('admin/class/edit/'.$id)->with('thanhcong','Sửa thành công');
    }
}
