<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classs;

class AjaxController extends Controller
{
    public function getClass($idGrade)
    {
    	$class = Classs::Where('grade_id',$idGrade)->get();
    	
    	foreach ($class as $c) {
    		echo '<option value='.$c->id.'>'. $c->class_name . '</option>';
    	}

    }
}
