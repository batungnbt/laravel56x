<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;
use App\Classs;
use Illuminate\Support\Facades\View;

class GradeController extends Controller
{
    
     public function __construct()
    {
        $class = Classs::all();
        $grade = Grade::all();
        View::share('class',$class);
        View::share('grade',$grade);

    }
	public function getAdd()
	{
		return view('grade.add');
	}

	public function postAdd(Request $request)
	{
		$validateData = $request->validate([
			'name' => 'required|unique:grade,grade_name',
		],
		[
			'name.required' => 'Bạn chưa nhập tên khối lớp',
			'name.unique' => 'Tên khối lớp đã tồn tại'
		]
	); 
		$grade = new Grade;
		$grade->grade_name = $request->name;
		$grade->save();
		return redirect('admin/grade/add')->with('thanhcong','Thêm mới thành công');


		
	}

	public function getList()
	{
		$grade = Grade::all();
		return view('grade.list',['grade' => $grade]);
	}

	public function delete($id)
	{
		$grade = Grade::find($id);
		$grade->delete();
		return redirect('admin/grade/list')->with('thongbao','Xóa thành công');
	}

	public function getEdit($id)
	{
		$grade = Grade::find($id);
		return view('grade.edit',['grade' => $grade]);
	}

	public function postEdit(Request $request,$id)
	{
		$grade = Grade::find($id);
		$validateData = $request->validate([
			'name' => 'required|unique:grade,grade_name',
		],
		[
			'name.required' => 'Bạn chưa nhập tên khối lớp',
			'name.unique' => 'Tên khối lớp đã tồn tại'
		]
	); 

		$grade->grade_name = $request->name;
		$grade->save();
		return redirect('admin/grade/edit/'.$id)->with('thanhcong','Sửa thành công');
	}
}
