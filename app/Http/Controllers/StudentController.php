<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Classs;
use App\Grade;
use Illuminate\Support\Facades\View;
class StudentController extends Controller
{
    
    public function __construct()
    {
        $class = Classs::all();
        $grade = Grade::all();
        View::share('class',$class);
        View::share('grade',$grade);

    }
    public function getAdd()
    {
    	
    	return view('student.add');
    }

    public function postAdd(Request $request)
    {
    	$validateData = $request->validate([
            'full_name' => 'required|min:3|max:100',
            'birth_day' => 'required',
            'birth_month' => 'required',
            'birth_year' => 'required',
            'address' => 'required',
            'avatar' => 'required|image',
            'class' => 'required',
            
        ],[
            'full_name.required' => 'Vui lòng nhập họ tên',
            'full_name.min' => 'Họ tên nhập yêu cầu nhập từ 3 đến 100 ký tự',
            'full_name.max' => 'Họ tên nhập yêu cầu nhập từ 3 đến 100 ký tự',
            'birth_day.required' => 'Vui lòng chọn ngày tháng năm sinh đầy đủ',
            'birth_month.required' => 'Vui lòng chọn ngày tháng năm sinh đầy đủ',
            'birth_year.required' => 'Vui lòng chọn ngày tháng năm sinh đầy đủ',
            'address.required' => 'Vui lòng nhập địa chỉ',
             'avatar.required' => 'Vui lòng chọn ảnh đại diện',
             'avatar.image' => 'Vui lòng chọn file ảnh',
            'class.required' => 'Vui lòng chọn lớp',
           

        ]);

        $student = new Student;
        $student->fullname = $request->full_name;
        $student->class_id = $request->class;
        $student->birthday = $request->birth_year . '-'.$request->birth_month . '-' . $request->birth_day;
        $student->sex = $request->sex;
        $student->address = $request->address;
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $fileExt = $file->extension();
            $fileName = str_random(8).'.'.$fileExt;
            $file->move('uploads',$fileName);
            $avatar = 'uploads/'.$fileName;
            echo $avatar;
        }
        $student->avatar = $avatar;
        $student->save();

        return redirect('admin/student/add')->with('thongbao','Thêm mới thành công');
    }

    public function getList()
    {   
        $student = Student::all();
        return view('student.list',['student' => $student]);
    }

    public function delete($id)
    {
        $student = Student::find($id);
        $student->delete();
        return redirect('admin/student/list')->with('thongbao','Xóa thành công');
    }

    public function getEdit($id)
    {
        $student = Student::find($id);
        return view('student.edit',['student' => $student]);
    }

    public function postEdit(Request $request,$id)
    {
        $validateData = $request->validate([
            'full_name' => 'required|min:3|max:100',
            'birth_day' => 'required',
            'birth_month' => 'required',
            'birth_year' => 'required',
            'address' => 'required',
            'avatar' => 'required|image',
            'class' => 'required',
            
        ],[
            'full_name.required' => 'Vui lòng nhập họ tên',
            'full_name.min' => 'Họ tên nhập yêu cầu nhập từ 3 đến 100 ký tự',
            'full_name.max' => 'Họ tên nhập yêu cầu nhập từ 3 đến 100 ký tự',
            'birth_day.required' => 'Vui lòng chọn ngày tháng năm sinh đầy đủ',
            'birth_month.required' => 'Vui lòng chọn ngày tháng năm sinh đầy đủ',
            'birth_year.required' => 'Vui lòng chọn ngày tháng năm sinh đầy đủ',
            'address.required' => 'Vui lòng nhập địa chỉ',
             'avatar.required' => 'Vui lòng chọn ảnh đại diện',
             'avatar.image' => 'Vui lòng chọn file ảnh',
            'class.required' => 'Vui lòng chọn lớp',
           

        ]);
        $student = Student::find($id);
        $student->fullname = $request->full_name;
        $student->class_id = $request->class;
        $student->birthday = $request->birth_year . '-'.$request->birth_month . '-' . $request->birth_day;
        $student->sex = $request->sex;
        $student->address = $request->address;
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $fileExt = $file->extension();
            $fileName = str_random(8).'.'.$fileExt;
            $file->move('uploads',$fileName);
            $avatar = 'uploads/'.$fileName;
            
        }else{
            $avatar = $request->avt;
        }
        $student->avatar = $avatar;
        $student->save();

        return redirect('admin/student/edit/'.$id)->with('thongbao','Sửa mới thành công');
    }
}
 