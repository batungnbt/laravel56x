<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    //
    public function __construct()
    {
    	$datas = [
	      'name' => 'Nguyễn Bá Tùng',
	      'age'  => '20',
	      'address' => 'Đại Hồng'
	  ];

    	View::share('datas',$datas);
    }
}
