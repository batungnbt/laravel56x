<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "Student";

    public function class()
    {
    	return $this->belongsTo('App\Classs','class_id','id');
    }
}
