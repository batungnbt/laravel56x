<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = "grade";

    public function class()
    {
    	return $this->hasMany('App\Class');
    }
}
