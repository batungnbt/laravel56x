<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classs extends Model
{
    protected $table = "class";


    public function grade()
    {
    	return $this->belongsTo('App\Grade','grade_id','id');
    }

    public function student()
    {
    	return $this->hasMany('App\Student');
    }
}
