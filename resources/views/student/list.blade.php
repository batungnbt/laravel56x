@extends('layout.index')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Student
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Avatar</th>
                                
                                <th>Full Name</th>
                                <th>Sex</th>
                                <th>BirthDay</th>
                                <th>Address</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($student as $st)
                            <tr class="odd gradeX" align="center">
                                <td>{{$st->id}}</td>
                                <td><img width="50" src="{{$st->avatar}}" alt=""></td>
                                
                                <td>{{$st->full_name}}</td>
                                <td>{{$st->sex}}</td>
                                <td>{{$st->birthday}}</td>
                                <td>{{$st->address}}</td>
                                
                                
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/student/edit/{{$st->id}}">Edit</a></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/student/delete/{{$st->id}}"> Delete</a></td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection