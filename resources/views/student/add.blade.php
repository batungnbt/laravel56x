@extends('layout.index')
@section('content')
<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Student
                        <small>Add</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        {{$err}} <br>
                        @endforeach
                    </div>
                    @endif
                    @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                    @endif
                    <form action="admin/student/add" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Full Name</label>
                            <input class="form-control" name="full_name" placeholder="Please Enter Full Name" />
                        </div>
                        <div class="form-group">
                            <label>Birthday</label>
                           
                            <select name="birth_day" class="">

                            <?php 
                              $start_date = 1;
                              $end_date   = 31;
                              for( $j=$start_date; $j<=$end_date; $j++ ) {
                                echo '<option value='.$j.'>'.$j.'</option>';
                              }
                            ?>
                          </select>
                            <select name="birth_month">
                            <?php for( $m=1; $m<=12; ++$m ) { 
                              $month_label = date('m', mktime(0, 0, 0, $m, 1));
                            ?>
                              <option value="<?php echo $month_label; ?>"><?php echo $month_label; ?></option>
                            <?php } ?>
                          </select> 
                        </span>
                        <span>
                          
                        </span>
                        <span>
                          <select name="birth_year">
                            <?php 
                              $year = date('Y');
                              $min = $year - 60;
                              $max = $year;
                              for( $i=$max; $i>=$min; $i-- ) {
                                echo '<option value='.$i.'>'.$i.'</option>';
                              }
                            ?>
                          </select>
                        </div>
                       <!--  end birth day -->
                        <div class="form-group">
                            <label>Sex</label> <br>
                            <label class="radio-inline">
                                <input name="sex" value="0" checked="" type="radio">Male
                            </label>
                            <label class="radio-inline">
                                <input name="sex" value="1" type="radio">Female
                            </label>
                        </div>
                        
                        <div class="form-group">
                            <label for="">Address</label>
                            <input class="form-control" name="address" placeholder="Please Enter Address" />
                        </div>
                        <div class="form-group">
                            <label for="Avatar">Avatar</label>
                            <input type="file" name="avatar" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Grade</label>
                            <select name="class" id="grade" class="form-control">
                                <option value="">--Select Grade--</option>
                                @foreach($grade as $g)
                                <option value="{{$g->id}}">{{$g->grade_name}}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Class</label>
                            <select name="class" id="class" class="form-control">
                                <option value="">--Select Class--</option>
                                @foreach($class as $c)
                                <option value="{{$c->id}}">{{$c->class_name}}</option>
                                @endforeach
                                
                            </select>
                        </div>
                        
                        <button type="submit" class="btn btn-default">Add</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#grade').change(function() {
            var idGrade = $(this).val();
            $.get('admin/ajax/class/'+idGrade, function(data) {
                $('#class').html(data);
            });
        });
    });
</script>
@endsection