@extends('layout.index')
@section('content')
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Class
                            <small>{{$class->class_name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    	@if(count($errors) > 0)
                    	<div class="alert alert-danger">
                    		
                    	
                    	@foreach($errors->all() as $err)
						{{$err}}<br>
                    	@endforeach
                    	</div>
                    	@endif
                    	@if(session('thanhcong'))
                    	<div class="alert alert-success">
                    	{{session('thanhcong')}} 
                    	</div>
                    	@endif
                        <form action="admin/class/edit/{{$class->id}}" method="POST">
                        	@csrf
                            <div class="form-group">
                                <label>Class Name</label>
                                <input class="form-control" name="name" placeholder="Please Enter Class Name" value="{{$class->class_name}}" />
                            </div>
                            <div class="form-group">
                            	<label for="Graden">Graden</label>
                            	<select name="grade" id="grade" class="form-control">
                            		<option value="">--Select Graden Name--</option>
                            		@foreach($grade as $g)
                            		<option 
                                    @if($class->grade_id == $g->id)
                                    {{'selected'}}
                                    @endif
                                    value="{{$g->id}}">{{$g->grade_name}}</option>
                            		@endforeach
                            		
                            	</select>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection

