<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(StudentSeeder::class);
    }


}

class StudentSeeder extends Seeder
{
	public function run()
	{
		DB::table('student')->insert(
		[
		['class_id' => rand(1,10),
		'fullname' => 'Nguyen '.str_random(6),
		'birthday' => '1998-12-14',
		'sex' => 1,'address' => 'dai hong',
		'avatar' => str_random(10)]
		]);
	}
}
